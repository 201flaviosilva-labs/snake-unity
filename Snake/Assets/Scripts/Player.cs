﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector2 direction = Vector2.right;
    private List<Transform> segments = new List<Transform>();

    [SerializeField] GameManager gameManager;
    [SerializeField] Transform segmentPrefab; // Snake Body
    [SerializeField] int initialSize = 2; // Start Size of the Snake

    void Start()
    {
        Restart();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) direction = Vector2.up;
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) direction = Vector2.down;
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) direction = Vector2.left;
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) direction = Vector2.right;
    }

    private void FixedUpdate()
    {
        for (int i = segments.Count - 1; i > 0; i--)
        {
            segments[i].position = segments[i - 1].position;
        }

        transform.position = new Vector3(
            Mathf.Round(transform.position.x + direction.x),
            Mathf.Round(transform.position.y + direction.y),
            Mathf.Round(transform.position.z)
            );
    }

    private void OnTriggerEnter2D(Collider2D otherGameObject)
    {
        if (otherGameObject.tag == "Food") Grow();
        else if (otherGameObject.tag == "Obstacle") Restart();

    }

    private void Restart()
    {
        gameManager.ResetGame();
        for (int i = 1; i < segments.Count; i++)
        {
            Destroy(segments[i].gameObject);
        }

        segments.Clear();
        segments.Add(transform);

        for (int i = 1; i < initialSize; i++)
        {
            segments.Add(Instantiate(segmentPrefab));
        }

        transform.position = Vector3.zero;
    }

    private void Grow()
    {
        gameManager.AddScore();
        Transform newSegment = Instantiate(segmentPrefab);
        newSegment.position = segments[segments.Count - 1].position;

        segments.Add(newSegment);
    }
}
