﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int score = 0;
    [SerializeField] Text scoreLabel;

    public void AddScore()
    {
        score++;
        scoreLabel.text = score.ToString();
    }

    public void ResetGame()
    {
        score = 0;
        scoreLabel.text = score.ToString();
    }
}
