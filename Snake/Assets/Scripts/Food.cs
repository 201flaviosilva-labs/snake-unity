﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{

    [SerializeField] BoxCollider2D spawnArea;

    void Start()
    {
        setRandomPosition();
    }

    private void setRandomPosition()
    {
        Bounds bounds = spawnArea.bounds;

        float x = Mathf.Round(Random.Range(bounds.min.x, bounds.max.x));
        float y = Mathf.Round(Random.Range(bounds.min.y, bounds.max.y));

        transform.position = new Vector3(x, y, transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D otherGameObject)
    {
        if (otherGameObject.tag == "Player" || otherGameObject.tag == "Obstacle") setRandomPosition();
    }
}
